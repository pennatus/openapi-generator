DOCKER_IMAGE := registry.gitlab.com/pennatus/openapi-generator/openapi-generator-cli:latest

build:
	docker build . -t ${DOCKER_IMAGE}

push:
	docker push ${DOCKER_IMAGE}
